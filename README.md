# HelloWorldReactJS

Learn abount ReactJS

# First tutorial
    *  [Pluralsight](url)
        https://app.pluralsight.com/player?course=react-js-getting-started&author=samer-buna&name=react-js-getting-started-m1&clip=3&mode=live
* [ ]  taskList
* 

# Must have

Node
React Chrome DevTool
Redux Chrome DevTool


# Librerie menzionate

React
Redux
Axios
Redux-sagas
React-intl
React-router
PropTypes
ExactPropTypes
Styled-components
Storybook
Eslint
Prettier


# Altre Librerie utili

Webpack-bundle-analyzer
React-Placeholder
Husky
Npm-check
Google-Map
Google-Map-Autocomplete
React-spring-animation


# Strumenti

Create-React-App
Bundlephobia


# Altro

Webpack
Babel
React-Native
React-Navigation
TypeScript
Git


Specifich